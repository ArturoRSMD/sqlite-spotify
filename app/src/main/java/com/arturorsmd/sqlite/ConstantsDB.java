package com.arturorsmd.sqlite;

public class ConstantsDB {

    static final String DATABASE_NAME = "SongsDB";
    static final int DATABASE_VERSION = 1;
    static final String TABLE_NAME = "Songs";
    static final String COLUMN_ID = "id";
    static final String COLUMN_NAME = "name";
    static final String COLUMN_ARTIST = "artist";


}
