package com.arturorsmd.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class SongSQLiteHelper extends SQLiteOpenHelper {

    String sqlCreate = "CREATE TABLE Songs(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT NOT NULL, artist TEXT NOT NULL)";
    String sqlDelete = "DROP TABLE IF EXISTS Songs";

    SongSQLiteHelper songSQLiteHelper;
    SQLiteDatabase database;

    public SongSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(sqlDelete);
        db.execSQL(sqlCreate);
    }

    public static void createNewSong(SQLiteDatabase database, String name, String artist){
        if (database != null){
            ContentValues contentValues = new ContentValues();
            contentValues.put("name", name);
            contentValues.put("artist", artist );
            database.insert("Songs", null, contentValues);
        }
    }

    public static  void deleteDatabase(SQLiteDatabase database){
        database.delete("Songs", "", null);


    }

    public static List<Song> songList(SQLiteDatabase database){
        List<Song> songs = new ArrayList<>();
        Cursor cursor = database.rawQuery("select * from Songs", null);



        if (cursor.moveToFirst()){
            while (cursor.isAfterLast() == false){
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String artist = cursor.getString(cursor.getColumnIndex("artist"));

                songs.add(new Song(id, name, artist));
                cursor.moveToNext();

            }
        }

        return songs;

    }

    public static void deleteSongWithName(SQLiteDatabase database, String name) {
        if (database != null) {
            String[] whereArgs = new String[] { name };
            database.delete("Songs", "name"+ "=?", whereArgs);
            database.close();
        }
    }

    public static  void deleteSongWithID(SQLiteDatabase database, int id){
        if (database != null){
            String[] whereArgs = new String[id];
            database.delete("Songs", "id"+ "=?", whereArgs);
        }
    }

    public static void updateNameSongWithName(SQLiteDatabase database, String currentName, String newName){
        ContentValues cv = new ContentValues();
        cv.put("name", newName);
        if (database != null){
            String[] whereArgs = new String[] {currentName};
            database.update("Songs", cv, "name"+ "=?", whereArgs);


            //database.execSQL("UPDATE Users SET name='hola' WHERE id=1 ");

        }
    }

    public static void updateArtistSongWithName(SQLiteDatabase database, String currentName, String newArtist){
        ContentValues cv = new ContentValues();
        cv.put("artist", newArtist);
        if (database != null){
            String[] whereArgs = new String[] {currentName};
            database.update("Songs", cv, "artist"+ "=?", whereArgs);


            //database.execSQL("UPDATE Users SET name='hola' WHERE id=1 ");

        }
    }




}
