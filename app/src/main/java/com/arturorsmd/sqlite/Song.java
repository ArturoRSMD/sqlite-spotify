package com.arturorsmd.sqlite;

public class Song {

    int id;
    String name;
    String artist;
    String coverPage;

    public Song(int id, String name, String artist) {
        this.name = name;
        this.artist = artist;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

}
