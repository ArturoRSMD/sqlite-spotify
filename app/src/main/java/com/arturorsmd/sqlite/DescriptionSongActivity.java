package com.arturorsmd.sqlite;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DescriptionSongActivity extends AppCompatActivity {

    TextView txtName;
    TextView txtArtist;
    Button buttonDelete;
    SongSQLiteHelper songSQLiteHelper;
    SQLiteDatabase database;
    EditText edtxtName;
    EditText edtxtArtist;
    LinearLayout llEdtxt;
    LinearLayout lLayoutMainContent;
    Button btnSave;
    Button btnCancel;
    Button btnEditData;
    RelativeLayout rLayoutEditContent;
    Bundle bundle;
    String name;
    String artist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description_song);

        txtName = findViewById(R.id.txt_description_name);
        txtArtist = findViewById(R.id.txt_description_artist);
        buttonDelete = findViewById(R.id.btn_delete);
        edtxtName = findViewById(R.id.edtxt_edit_name);
        edtxtArtist = findViewById(R.id.edtxt_edit_artist);
        llEdtxt = findViewById(R.id.ll_editext);
        lLayoutMainContent = findViewById(R.id.ll_txt);
        btnSave = findViewById(R.id.btn_save_data);
        btnCancel = findViewById(R.id.btn_cancel_data);
        btnEditData = findViewById(R.id.btn_update_data);
        rLayoutEditContent = findViewById(R.id.btns_edit);

        initSetup();

        btnEditData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               hideMainContentLayout();
               showEditContentLayout();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideEditContentLayout();
                showMainContentLayout();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edtxtName.getText().toString().equals("") && !edtxtArtist.getText().toString().equals("")){
                    updateNameAndArtist();
                } else if (!edtxtName.getText().toString().equals("") && edtxtArtist.getText().toString().equals("")){
                    updateNameSong();
                } else if (edtxtName.getText().toString().equals("") && !edtxtArtist.getText().toString().equals("")){
                    updateArtistSong();
                }

            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteSong();
            }
        });



    }


    public void hideMainContentLayout(){
        lLayoutMainContent.setVisibility(View.GONE);
        edtxtName.setHint(name);
        edtxtArtist.setHint(artist);
    }

    public void showEditContentLayout(){
        rLayoutEditContent.setVisibility(View.VISIBLE);
    }

    public void hideEditContentLayout(){
        rLayoutEditContent.setVisibility(View.GONE);
    }

    public void showMainContentLayout(){
        lLayoutMainContent.setVisibility(View.VISIBLE);
        edtxtName.setHint(name);
        edtxtArtist.setHint(artist);
    }

    public void deleteSong(){
        SongSQLiteHelper.deleteSongWithName(database, name);
        Intent intent = new Intent(DescriptionSongActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void updateNameAndArtist(){
        SongSQLiteHelper.updateNameSongWithName(database, name,  edtxtName.getText().toString());
        SongSQLiteHelper.updateArtistSongWithName(database, artist, edtxtArtist.getText().toString());
        Intent intent = new Intent(DescriptionSongActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void updateNameSong(){
        SongSQLiteHelper.updateNameSongWithName(database, name,  edtxtName.getText().toString());
        Intent intent = new Intent(DescriptionSongActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void updateArtistSong(){
        SongSQLiteHelper.updateArtistSongWithName(database, artist, edtxtArtist.getText().toString());
        Intent intent = new Intent(DescriptionSongActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void initSetup(){
        sqLiteSetup();
        bundle = getIntent().getExtras();
        name = bundle.getString("name");
        artist = bundle.getString("artist");
        txtName.setText(name);
        txtArtist.setText(artist);
    }

    public void sqLiteSetup(){
        songSQLiteHelper = new SongSQLiteHelper(this, "Songs", null, 1);
        database = songSQLiteHelper.getWritableDatabase();
        database = songSQLiteHelper.getReadableDatabase();
    }
}
