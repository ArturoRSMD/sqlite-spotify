package com.arturorsmd.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    SongSQLiteHelper songSQLiteHelper;
    SQLiteDatabase database;
    LinearLayoutManager linearLayoutManager;
    Button btnAddSong;
    EditText edtxtName;
    EditText edtxtArtist;
    SongAdapter songAdapter;
    ImageView imgCoverPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final RecyclerView recyclerViewSong =  findViewById(R.id.rc_view_songs);
        btnAddSong = findViewById(R.id.btn_create);
        edtxtName = findViewById(R.id.edtxt_name);
        edtxtArtist = findViewById(R.id.edtxt_artist);
        imgCoverPage = findViewById(R.id.img_cover_page);


        linearLayoutManager = new LinearLayoutManager(this);

        songSQLiteHelper = new SongSQLiteHelper(this, "Songs", null, 1);
        database = songSQLiteHelper.getWritableDatabase();
        database = songSQLiteHelper.getReadableDatabase();

        imgCoverPage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                SongSQLiteHelper.deleteDatabase(database);



                songAdapter = new SongAdapter(getBaseContext(), SongSQLiteHelper.songList(database));
                recyclerViewSong.setAdapter(songAdapter);

                return false;


            }
        });






        recyclerViewSong.setLayoutManager(linearLayoutManager);
        recyclerViewSong.setHasFixedSize(true);
        songAdapter = new SongAdapter(this, SongSQLiteHelper.songList(database));
        recyclerViewSong.setAdapter(songAdapter);



        btnAddSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtxtName.getText().toString().equals("") && edtxtArtist.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, "Campos vacíos", Toast.LENGTH_SHORT).show();
                } else{

                    SongSQLiteHelper.songList(database).clear();
                    //createSong(edtxtName.getText().toString(), edtxtArtist.getText().toString());
                    SongSQLiteHelper.createNewSong(database, edtxtName.getText().toString(), edtxtArtist.getText().toString());
                    songAdapter = new SongAdapter(getBaseContext(), SongSQLiteHelper.songList(database));
                    recyclerViewSong.setAdapter(songAdapter);

                    edtxtName.getText().clear();
                    edtxtArtist.getText().clear();

                }







            }
        });



    }






    @Override
    protected void onDestroy() {

        database.close();

        super.onDestroy();
    }


}
